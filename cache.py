import random
import sys

class Cache:

    def __init__(self, block_size, size, mem_size, mapping_selected, set_size, replacement_policy):
        #self._lines = [-1] * size
        self._lines = dict((i,-1) for i in range(size))
        #self._contadoraux = [0] * size
        self._contadoraux = dict((i,0) for i in range(size))
        self._full = False
        self._totalcount = 0
        self._existBigger = False
        self._higher_position = 0
        #self._content = [0] * (mem_size*size)
        self._content = dict((i,0) for i in range(mem_size*size))
        self._custom_message = ""

        self._block_size = block_size
        self._size = size
        self._mem_size = mem_size
        self._mapping_selected = mapping_selected
        self._set_size = set_size
        self._replacement_policy = replacement_policy
        
    def random_int( self ):
        return random.randint(0, sys.maxsize)

    def read( self, palavra ):
        
        if self._mapping_selected == 1:
            self.mapeamentoDireto( palavra )
        elif self._mapping_selected == 2:
            self.totalmenteAssociativo( palavra )
        elif self._mapping_selected == 3:
            self.parcialmenteAssociativo( palavra )

    def write( self, palavra, nova_palavra ):
        
        is_new = self._content[palavra] != nova_palavra
        self._content[palavra] = nova_palavra

        if( is_new ):
            self._custom_message = f" -> novo valor do endereco {palavra}={nova_palavra}"
        else:
            self._custom_message = ""

        self.read(palavra)
        self._custom_message = ""

    def totalmenteAssociativo( self, palavra ):
        if(self._replacement_policy == 1):
            self.totalmenteAleatorio(palavra)
        elif(self._replacement_policy == 2):
            self.totalmentefifo( palavra )
        elif(self._replacement_policy == 3):
            self.totalmentelfu( palavra )

    def parcialmenteAssociativo( self, palavra ):

        if(self._replacement_policy == 1):
            self.parcialmenteAleatorio( palavra )
        elif(self._replacement_policy == 2):
            self.parcialmenteFIFO( palavra )
        elif(self._replacement_policy == 3):
            self.parcialmentelfu( palavra )

    def parcialmentelfu( self, palavra ):
    
        bloco = palavra // self._block_size
        qtVias = self._set_size
        via = bloco % qtVias
        linhaspv = self._size // self._set_size
        auxx = via*linhaspv

        for i in range( self._size ):
            if(self._lines[i] == bloco):
                print("HIT linha " + str((i % linhaspv)) + self._custom_message)
                self._contadoraux[i] += 1
                return

        for j in range(auxx, (auxx + linhaspv)):
            if(self._lines[j] == -1):
                self._lines[j] = bloco
                self._contadoraux[j] += 1
                print(f"MISS -> alocado na linha {j % linhaspv} -> bloco {bloco} substituido")
                if(j == ((auxx) + linhaspv)-1):
                    self._full = True
                return


        if(self._full == True):
            menor = self.menorpa(auxx)
            aux = 0
            aux2 = 0
            for i in range(auxx, ((auxx) + linhaspv)):
                if(self._contadoraux[i] == menor):
                    aux += 1
                    aux2 = i

            if(aux > 1):
                for i in range(auxx, ((auxx) + linhaspv)):
                    if(self._contadoraux[i] == menor):
                        self._lines[i] = bloco
                        self._contadoraux[i] += 1
                        print(f"MISS -> alocado na linha {i % linhaspv} -> bloco {bloco} substituido")
                        return
            elif(aux == 1):
                self._lines[aux2] = bloco
                self._contadoraux[aux2] += 1
                print(f"MISS -> alocado na linha {aux2 % linhaspv} -> bloco {bloco} substituido")
                return

    def parcialmenteFIFO( self, palavra ):
        linhaspv = self._size // self._set_size
        bloco = palavra // self._block_size
        qtVias = self._set_size
        via = bloco % qtVias
        auxx = via*linhaspv

        for i in range( self._size ):
            if(self._lines[i] == bloco):
                print("HIT linha " + str((i % linhaspv)) + self._custom_message)
                return
        

        for j in range(auxx, ((auxx) + linhaspv)):
            self._contadoraux[j] += 1
            if(self._lines[j] == -1):
                self._lines[j] = bloco
                print(f"MISS -> alocado na linha {j % linhaspv} -> bloco {bloco} substituido")
                if(j == ((auxx) + linhaspv)-1):
                    self._full = True
                return
        
        if(self._full == True):
            maior = self.maiorta()
            for i in range(auxx, ((auxx) + linhaspv)):
                if(self._contadoraux[i] == maior):
                    self._lines[i] = bloco
                    self._contadoraux[i] = 1
                    print(f"MISS -> alocado na linha {i % linhaspv} -> bloco {bloco} substituido")
                    return

    def menorpa( self, iniciovia ):
        menor = self._contadoraux[iniciovia]

        for i in range(iniciovia + 1, (iniciovia + (self._size // self._set_size))):
            if(self._contadoraux[i] < menor):
                menor = self._contadoraux[i]
        
        return menor
    

    def mapeamentoDireto( self, palavra ):

        bloco = palavra // self._block_size
        linha = bloco % self._size

        if self._lines[linha] == bloco:
            print("HIT linha " + str(linha) + self._custom_message)
        else:
            self._lines[linha] = bloco
            print(f"MISS -> alocado na linha {linha} -> bloco {bloco} substituido")

    def parcialmenteAleatorio( self, palavra ):
        qtddevias = self._set_size
        linhaspv = self._size // self._set_size
        bloco = palavra // self._block_size
        via = self.random_int() % qtddevias
        linha = self.random_int() % linhaspv

        for i in range( self._size ):
            if self._lines[i] == bloco:
                print(f"HIT linha {i%linhaspv}" + self._custom_message)
                return

        self._lines[linha + linhaspv*via] = bloco
        print(f"MISS -> alocado na linha {linha + linhaspv*via} -> bloco {bloco} substituido")

    def totalmenteAleatorio( self, palavra ):
        bloco = palavra // self._block_size
        linha = self.random_int() % self._size

        for i in range( self._size ):
            if self._lines[i] == bloco:
                print("HIT linha " + str(i) + self._custom_message)
                return

        self._lines[linha] = bloco
        print(f"MISS -> alocado na linha {linha} -> bloco {bloco} substituido")

    def maiorta( self ):
        maior = 0
        for i in range( self._size ):
            if maior < self._contadoraux[i]:
                maior = self._contadoraux[i]
        
        return maior
    
    def menorta( self ):
        menor = self._contadoraux[0]
        for i in range( self._size ):
            if(self._contadoraux[i] < menor):
                menor = self._contadoraux[i]

        return menor

    def totalmentelfu( self, palavra ):
        bloco = palavra // self._block_size
        
        for i in range( self._size ):
            if self._lines[i] == bloco:
                print("HIT linha " + str(i) + self._custom_message)
                self._contadoraux[i] += 1
                return

        menor = self.menorta()
        for i in range( self._size ):
            if menor == 0 and self._lines[i]==-1:
                self._lines[i] = bloco
                self._contadoraux[i] = 1
                print( f"MISS -> alocado na linha {i} -> bloco {bloco} substituido")

                if i == self._size-1:
                    self._full = True
                return            
           
        if menor != 0 and self._full == True:
            aux = 0
            for i in range( self._size ):
                if self._contadoraux[i] == menor:
                    aux += 1
                
            
            if aux > 1:
                for i in range( self._size ):
                    if self._contadoraux[i] == menor:
                        self._lines[i] = bloco
                        self._contadoraux[i] += 1
                        print( f"MISS -> alocado na linha {i} -> bloco {bloco} substituido")
                        return
                    
                
            elif aux == 1:
                for i in range( self._size ):
                    if self._contadoraux[i] == menor:
                        self._lines[i]=bloco
                        self._contadoraux[i]+=1
                        print( f"MISS -> alocado na linha {i} -> bloco {bloco} substituido")
                return

    def totalmentefifo( self, palavra ):
        bloco = palavra // self._block_size

        for i in range( self._size ):
            if self._lines[i] == bloco:
                print("HIT linha " + str(i) + self._custom_message)
                return

        maior = self.maiorta()

        for j in range( self._size ):
            self._contadoraux[j] += 1
            if maior == 0:
                self._lines[0] = bloco
                print( f"MISS -> alocado na linha 0 -> bloco {bloco} substituido")
                return

            if(self._lines[j] == -1):
                self._lines[j] = bloco
                print("HIT linha " + str(j) + self._custom_message)
                if j == self._size-1:
                    self._full = True
                return

        if maior != 0 and self._full == True:
            maior = self.maiorta()
            for i in range( self._size ):
                if self._contadoraux[i] == maior:
                    self._lines[i] = bloco
                    self._contadoraux[i] = 1
                    print( f"MISS -> alocado na linha {i} -> bloco {bloco} substituido")
                    return

    def show(self):
        print("CACHE L1")
        print("Linha -Bloco -Endereco -Conteudo")
        for i in range( self._size ):
            for z in range( self._size ):
                if(self._lines[i] > -1):
                    print( str(i) + " - " + str(self._lines[i]) + " - " + str( (self._lines[i]*self._size+z) ) + " - " + str( ( self._content[self._lines[i]*self._size+z])) )
                else:
                    print( str(i) + " - x - x - x" )
                   
        print("MEMORIA PRINCIPAL:")
        print("Bloco -Endereco -Conteudo")
        for i in range( self._block_size * self._mem_size ):
            print( str( (i//self._size)) + " - " + str(i) + " - " + str(self._content[i]))