necessário python 3.6

execute com o comando:

```
python simulador.py
```

Comandos:

```
Read <address>
Write <address> <new_value>
Show
Quit
```

Arquivo **config.txt**:
```
Tamanho do bloco (em número de palavras)
Numero de linhas da cache
Numero de blocos da memória principal
Mapeamento (1 –Direto; 2 –Totalmente Associativo; 3 –Parcialmente Associativo)
Numero de conjuntos (caso não seja Parcialmente Associativo, ler o valor normalmente mas desconsidere-o)
Política de substituição (1 –Aleatório; 2 –FIFO; 3 –LFU)
```

Arquivo **instrucoes.txt** pode definir instruções para executar junto do programa.