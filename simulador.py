#encoding: utf-8
import os
from cache import Cache

def main():
    cache = Cache(block_size, cache_size, mem_size, mapping_selected, set_size, replacement_policy)
    print("====== instrucoes ====== ")

    with open(os.path.join(__location__, 'instrucoes.txt')) as f:
        content = f.readlines()

    config = [x.strip() for x in content] 
    for operation in config:
        operation = operation.split(" ")
        command = operation[0]

        if( command == "read" ):
            cache.read( int(operation[1]) )

        elif( command == "write" ):
            cache.write( int(operation[1]), int(operation[2]) )

        elif( command == "show" ):
            cache.show()

    print("\n\n==================================================")
    print("Comandos: ")
    print(" - READ <endereco>")
    print(" - WRITE <endereco> <valor>")
    print(" - SHOW")
    print(" - QUIT")
    print("=========== AGUARDANDO COMANDOS =============")
    command = ""
    while (command.lower() != "quit"):
        operation = input()
        operation = operation.split(" ")
        command = operation[0]

        if( command == "read" ):
            cache.read( int(operation[1]) )

        elif( command == "write" ):
            cache.write( int(operation[1]), int(operation[2]) )

        elif( command == "show" ):
            cache.show()
    
    print("================== FIM ==================")


if __name__ == '__main__':

    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

    with open(os.path.join(__location__, 'config.txt')) as f:
        content = f.readlines()

    config = [x.strip() for x in content] 

    block_size = int(config[0])
    cache_size = int(config[1])
    mem_size = int(config[2])
    mapping_selected = int(config[3])
    set_size = int(config[4])
    replacement_policy = int(config[5])

    main()